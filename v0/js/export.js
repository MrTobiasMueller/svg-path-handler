// EXPORT
// https://levelup.gitconnected.com/draw-an-svg-to-canvas-and-download-it-as-image-in-javascript-f7f7713cf81f

let svgElements = document.getElementsByClassName('artboard');
let svgElement = svgElements[0]

let {width, height} = svgElement.getBBox()

let clonedSvgElement = svgElement.cloneNode(true)
// true for deep clone

let outerHTML = clonedSvgElement.outerHTML
let blob = new Blob([outerHTML],{type:'image/svg+xml;charset=utf-8'})

let url = window.URL || window.webkitURL || window
let blobURL = URL.createObjectURL(blob)

let imageA = new Image()
let imageB = new Image()

let scaleFactorA = 2
let scaleFactorB = 3

imageA.onload = () => {
    let canvasA = document.createElement('canvas')
    canvasA.width = width * scaleFactorA
    canvasA.height = height * scaleFactorA

    let contextA = canvasA.getContext('2d')
    contextA.drawImage(imageA, 0, 0, width * scaleFactorA, height * scaleFactorA)

    document.getElementById('canvas-wrapper-1').appendChild(canvasA);

    // downloadImage(canvas)
}

imageB.onload = () => {
    let canvasB = document.createElement('canvas')
    canvasB.width = width * scaleFactorB
    canvasB.height = height * scaleFactorB

    let contextB = canvasB.getContext('2d')
    contextB.drawImage(imageB, 0, 0, width * scaleFactorB, height * scaleFactorB)

    document.getElementById('canvas-wrapper-2').appendChild(canvasB);

    // downloadImage(canvas)
}

imageA.src = blobURL
imageB.src = blobURL

// https://stackoverflow.com/questions/23068788/get-image-data-from-blob-from-the-img-tag-opposite-method-to-url-createobject?rq=1