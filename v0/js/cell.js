// CELL
// MAZE CREATION
const neighborsChecker = state => ({
  checkNeighbors(maze) {
    const {x, y} = state
    let neighbors = []
    
    let top, right, bottom, left
    let current = maze.grid[x][y]
    if (y > 0) {
      top = maze.grid[x][y-1]
      if (!top.visited && current.static[0]==false) {
        neighbors.push(top)
      }
    }
    if (x < maze.cells[0]-1) {
      right = maze.grid[x+1][y]
      if (!right.visited && current.static[1]==false) {
        neighbors.push(right)
      }
    }
    if (y < maze.cells[1]-1) {
      bottom = maze.grid[x][y+1]
      if (!bottom.visited && current.static[2]==false) {
        neighbors.push(bottom)
      }
    }
    if (x > 0) {
      left = maze.grid[x-1][y]
      if (!left.visited && current.static[3]==false) {
        neighbors.push(left)
      }
    }
    
    if (neighbors.length > 0) {
      let r = Math.floor(Math.random()*neighbors.length)
      return neighbors[r]
    }
    if (neighbors.length <= 0) {
      return undefined
    }
    
  }
})

const highlighter = state => ({
  highlight(container, color, classNames) {
    container.rect([state.x*state.width+3, state.y*state.height+3], [state.width-6, state.height-6], color, '#ccc', 0, 'round', classNames, '')
  }
})
const wallDrawer = state => ({
  drawWalls(container, cellSize) {
    let x = state.x*cellSize[0]
    let y = state.y*cellSize[1]

    let wallColor = '#777'
    let wallThickness = 1

    let wallColorStatic = '#f77'
    let wallThicknessStatic = 3

    if (state.walls[0]) {
      if (state.static[0]) {
        container.line([x, y], [x+cellSize[0], y], wallColorStatic, wallThicknessStatic, 'round', 'wall--top')
      } else {
        container.line([x, y], [x+cellSize[0], y], wallColor, wallThickness, 'round', 'wall--top')
      }
    }
    if (state.walls[1]) {
      if (state.static[1]) {
        container.line([x+cellSize[0], y], [x+cellSize[0], y+cellSize[1]], wallColorStatic, wallThicknessStatic, 'round', 'wall--right')
      } else {
        container.line([x+cellSize[0], y], [x+cellSize[0], y+cellSize[1]], wallColor, wallThickness, 'round', 'wall--right')
      }
    }
    if (state.walls[2]) {
      if (state.static[2]) {
        container.line([x+cellSize[0], y+cellSize[1]], [x, y+cellSize[1]], wallColorStatic, wallThicknessStatic, 'round', 'wall--bottom')
      } else {
        container.line([x+cellSize[0], y+cellSize[1]], [x, y+cellSize[1]], wallColor, wallThickness, 'round', 'wall--bottom')
      }
    }
    if (state.walls[3]) {
      if (state.static[3]) {
        container.line([x, y+cellSize[1]], [x, y], wallColorStatic, wallThicknessStatic, 'round', 'wall--left')
      } else {
        container.line([x, y+cellSize[1]], [x, y], wallColor, wallThickness, 'round', 'wall--left')
      }
    }
    //if (state.visited) {}
  }
})

// PATH FINDING --------------------------------------
const neighborsAdder = state => ({
  addNeighbors(maze) {
    let {x, y} = state
    // right, left, bottom, top
    if (x < maze.cells[0] - 1 && state.walls[1] == false) {
      state.mazeNeighbors.push(maze.grid[x+1][y])
    }
    if (x > 0 && state.walls[3] == false) {
      state.mazeNeighbors.push(maze.grid[x-1][y])
    }
    if (y < maze.cells[1] - 1 && state.walls[2] == false) {
      state.mazeNeighbors.push(maze.grid[x][y+1])
    }
    if (y > 0 && state.walls[0] == false) {
      state.mazeNeighbors.push(maze.grid[x][y-1])
    }
  }
})

const arcSegmentCreater = state => ({
  arcSegment(segName, large) {

    // Als Globale Variablen?
    // const arcSize = [state.width/2, state.height/2]
    // const a_tr = arc('a', [[arcSize[0],arcSize[1]],0,0,0,[arcSize[0],arcSize[1]]], 'a_tr')
    // const a_tr_l = arc('a', [[arcSize[0],arcSize[1]],0,1,0,[arcSize[0],arcSize[1]]], 'a_tr_l')
    // const a_br = arc('a', [[arcSize[0],arcSize[1]],0,0,0,[-arcSize[0],arcSize[1]]], 'a_br')
    // const a_br_l = arc('a', [[arcSize[0],arcSize[1]],0,1,0,[-arcSize[0],arcSize[1]]], 'a_br_l')
    // const a_bl = arc('a', [[arcSize[0],arcSize[1]],0,0,0,[-arcSize[0],-arcSize[1]]], 'a_bl')
    // const a_bl_l = arc('a', [[arcSize[0],arcSize[1]],0,1,0,[-arcSize[0],-arcSize[1]]], 'a_bl_l')
    // const a_tl = arc('a', [[arcSize[0],arcSize[1]],0,0,0,[arcSize[0],-arcSize[1]]], 'a_tl')
    // const a_tl_l = arc('a', [[arcSize[0],arcSize[1]],0,1,0,[arcSize[0],-arcSize[1]]], 'a_tl_l')

    state.segment = segment(segName)

    let commands

    if (segName === 'tl_convex') {
      if (large) { commands = [a_bl, a_tl, a_tr] }
      if (!large) {  commands = [a_tl] }
    }
    if (segName === 'tl_concave') {
      if (large) { commands = [a_tl_l] }
      if (!large) { commands = [a_tl] }
    }
    if (segName === 'ct') {
      if (large) { commands = [a_tl, a_tr] }
      if (!large) { commands = [a_tr, a_tl] }
    }
    if (segName === 'tr_convex') {
      if (large) { commands = [a_tl, a_tr, a_br] }
      if (!large) { commands = [a_tr] }
    }
    if (segName === 'tr_concave') {
      if (large) {  commands = [a_tr_l] }
      if (!large) {  commands = [a_tr] }
    }
    if (segName === 'cr') {
      if (large) { commands = [a_tr, a_br] }
      if (!large) { commands = [a_br, a_tr] }
    }
    if (segName === 'br_convex') {
      if (large) { commands = [a_tr, a_br, a_bl] }
      if (!large) { commands = [a_br] }
    }
    if (segName === 'br_concave') {
      if (large) { commands = [a_br_l] }
      if (!large) { commands = [a_br] }
    }
    if (segName === 'cb') {
      if (large) { commands = [a_br, a_bl] }
      if (!large) { commands = [a_bl,  a_br] }
    }
    if (segName === 'bl_convex') {
      if (large) { commands = [a_br, a_bl, a_tl] }
      if (!large) { commands = [a_bl] }
    }
    if (segName === 'bl_concave') {
      if (large) { commands = [a_bl_l] }
      if (!large) {  commands = [a_bl] }
    }
    if (segName === 'cl') {
      if (large) { commands = [a_bl, a_tl] }
      if (!large) { commands = [a_tl, a_bl] }
    }

    state.segment.addCommands(commands)

  }
})

const grammarApplyer = state => ({
  applyGrammar() {
    // TOP LEFT
    if (state.prevBottom && state.nextRight) {
      state.arcSegment('tl_convex', state.largeSegment)
    }
    if (state.prevLeft && state.nextTop) {
      state.arcSegment('tl_concave', state.largeSegment)
    }
    // CENTER TOP
    if (state.prevLeft && state.nextRight) {
      state.arcSegment('ct', state.largeSegment)
    }
    // TOP RIGHT
    if (state.prevLeft && state.nextBottom) {
      state.arcSegment('tr_convex', state.largeSegment)
    }
    if (state.prevTop && state.nextRight) {
      state.arcSegment('tr_concave', state.largeSegment)
    }
    // CENTER RIGHT
    if (state.prevTop && state.nextBottom) {
      state.arcSegment('cr', state.largeSegment)
    }
    // BOTTOM RIGHT
    if (state.prevTop && state.nextLeft) {
      state.arcSegment('br_convex', state.largeSegment)
    }
    if (state.prevRight && state.nextBottom) {
      state.arcSegment('br_concave', state.largeSegment)
    } 
    // CENTER BOTTOM
    else if (state.prevRight && state.nextLeft) {
      state.arcSegment('cb', state.largeSegment)
    } 
    // BOTTOM LEFT
    if (state.prevRight && state.nextTop) {
      state.arcSegment('bl_convex', state.largeSegment)
    }
    if (state.prevBottom && state.nextLeft) {
      state.arcSegment('bl_concave', state.largeSegment)
    }
    // CENTER LEFT
    if (state.prevBottom && state.nextTop) {
      state.arcSegment('cl', state.largeSegment)
    }
    else {
      console.log('skipped segment')
    }
  }
})

const cell = (x, y, width, height, xPosition, yPosition) => {
  const self = {
    // MAZE
    x,
    y,
    width,
    height,
    xPosition,
    yPosition,
    walls: [true,true,true,true],
    static: [false,false,false,false],
    visited: false,
    // TRACE FINDING
    f: 0,
    g: 0, // actual "cost" from beginning to end
    h: 0, // heuristics
    mazeNeighbors: [],
    previous: undefined,
    blocked: false, // wird schon gesetzt, aber noch nicht genutzt
    // TRACE DRAWING
    prev: undefined,
    next: undefined,
    prevTop: false,
    prevRight: false,
    prevBottom: false,
    prevLeft: false,
    nextTop: false,
    nextRight: false,
    nextBottom: false,
    nextLeft: false,
    absoluteTop: undefined,
    absoluteRight: undefined,
    absoluteBottom: undefined,
    absoluteLeft: undefined,
    absoluteNeighbors: [],
    largeSegment: false,
    segment: undefined
  }
  return Object.assign(
    self,
    neighborsChecker(self),
    highlighter(self),
    wallDrawer(self),
    // A Star
    neighborsAdder(self),
    arcSegmentCreater(self),
    grammarApplyer(self)
  )
}















// CELL P5
/*
const neighborsChecker = state => ({
  checkNeighbors() {
    const {x, y} = state
    let neighbors = []
    
    let top, right, bottom, left
    if (y > 0) {
      top = grid[x][y-1]
      if (!top.visited) {
        neighbors.push(top)
      }
    }
    if (x < cols-1) {
      right = grid[x+1][y]
      if (!right.visited) {
        neighbors.push(right)
      }
    }
    if (y < rows-1) {
      bottom = grid[x][y+1]
      if (!bottom.visited) {
        neighbors.push(bottom)
      }
    }
    if (x > 0) {
      left = grid[x-1][y]
      if (!left.visited) {
        neighbors.push(left)
      }
    }
    
    if (neighbors.length > 0) {
      let r = Math.floor(Math.random()*neighbors.length)
      return neighbors[r]
    } else {
      return undefined
    }
    
  }
})
const highlighter = state => ({
  highlight(col) {
    noStroke()
    fill(col)
    rect(state.x * w, state.y * h, w, h)
  }
})
const wallDrawer = state => ({
  // WALLSHOWER
  drawWalls() {
    let x = state.x*w
    let y = state.y*h
    stroke(255)
    //noStroke()
    if (state.walls[0]) {
      line(x,y,x+w,y)
    }
    if (state.walls[1]) {
      line(x+w,y,x+w,y+h)
    }
    if (state.walls[2]) {
      line(x+w,y+h,x,y+h)
    }
    if (state.walls[3]) {
      line(x,y+h,x,y)
    }
    //if (state.visited) {
    //  noStroke()
    //  fill(55,55,55)
    //  rect(x,y,w,h)
    //}
  }
})

// A STAR --------------------------------------
const neighborsAdder = state => ({
  addNeighbors(grid) {
    let {x, y} = state
    // right, left, bottom, top
    if (x < cols - 1 && state.walls[1] == false) {
      state.neighbors.push(grid[x+1][y])
    }
    if (x > 0 && state.walls[3] == false) {
      state.neighbors.push(grid[x-1][y])
    }
    if (y < rows - 1 && state.walls[2] == false) {
      state.neighbors.push(grid[x][y+1])
    }
    if (y > 0 && state.walls[0] == false) {
      state.neighbors.push(grid[x][y-1])
    }
  }
})
*/