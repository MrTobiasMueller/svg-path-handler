// https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


// MAZEPATH
function removeFromArray(arr, element) {
  // Gehört zu A Star
  for (let i = arr.length-1; i >=0; i--) {
    if (arr[i] == element) {
      arr.splice(i, 1)
    }
  }
}
function heuristic(a, b) {
  // Gehört zu A Star
  // dist ist eine p5-Funktion
  // let d = dist(a.x,a.y,b.x,b.y) // Euclidian
  let d = Math.abs(a.x-b.x) + Math.abs(a.y-b.y)
  return d
}
  
// CELL
// GEHÖRT ZU CREATEMAZE GENERATOR IN MAZEPATH
// Ist keine Methode von cell, da sie zwei cells als Argumente nimmt.
const removeWalls = (a, b) => {
  let x = a.x - b.x  
  if (x === 1) {
    a.walls[3] = false
    b.walls[1] = false
  } else if (x === -1) {
    a.walls[1] = false
    b.walls[3] = false
  }
  let y = a.y - b.y
  if (y === 1) {
    a.walls[0] = false
    b.walls[2] = false
  } else if (y === -1) {
    a.walls[2] = false
    b.walls[0] = false
  }
}

// GEHÖRT ZU CREATESEGMENT IN MAZEPATH
// STARTPOINT PATH
const setStartPoint = (cellSize, startCell, moveTo) => {
  let cellW = cellSize[0]
  let cellH = cellSize[1]
  // top, right, bottom left as a position on the start cell
  if (moveTo === 'top') { // FIT MAZE – UPSIDE DOWN
    return move('M', [[startCell.x*cellW + cellW/2, startCell.y*cellH]], 'startPoint')
  } else if (moveTo === 'topRight') {
    return move('M', [[startCell.x*cellW + cellW, startCell.y*cellH]], 'startPoint')
  } else if (moveTo === 'right') {
    return move('M', [[startCell.x*cellW + cellW, startCell.y*cellH + cellH/2]], 'startPoint')
  } else if (moveTo === 'bottomRight') {
    return move('M', [[startCell.x*cellW + cellW, startCell.y*cellH + cellH]], 'startPoint')
  } else if (moveTo === 'bottom') { // FIT MAZE – NORMAL
    return move('M', [[startCell.x*cellW + cellW/2, startCell.y*cellH + cellH]], 'startPoint')
  } else if (moveTo === 'bottomLeft') {
    return move('M', [[startCell.x*cellW, startCell.y*cellH + cellH]], 'startPoint')
  } else if (moveTo === 'left') {
    return move('M', [[startCell.x*cellW, startCell.y*cellH + cellH/2]], 'startPoint')
  } else if (moveTo === 'topLeft') {
    return move('M', [[startCell.x*cellW, startCell.y*cellH]], 'startPoint')
  } else if (moveTo === 'center') {
    return move('M', [[startCell.x*cellW + cellW/2, startCell.y*cellH + cellH/2]], 'startPoint')
  } else {
    // NO STARTPOINT ADDED
    console.log('no startPoint for path in mazePath')
  }
}
