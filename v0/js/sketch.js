// SEGMENTS SOLLTEN IHRE HÖHE UND BREITE KENNEN
// SOWAS WIE INIT EINRICHTEN
// MAZEPATH AUFSPLITTEN
// ALLGEMEIN OBJEKTE ALS OUTPUT DER WEITERGEREICHT WIRD
// MIRROR
// CLIPPATH
// Austausch sich wiederholender Elemente durch größere, wenn genug Platz (BLOCKED CELLS BENUTZEN)
// COMMANDS ALS SEGMENTE AUS ARCS MIT SKALIERTEN RANDOM-GRÖSSEN
// STRUKTUR PAPIER

// REFAKTORIEREN: https://www.youtube.com/watch?v=EumXak7TyQ0
// ENUMS: https://masteringjs.io/tutorials/fundamentals/enum

// ARTBOARD -----------------------------------
const wrapper = document.getElementById("svg-wrapper-1");

const compSize = [600, 600];
const artboardOffset = [0, 0];

const artboardSize = [compSize[0] + artboardOffset[0], compSize[1] + artboardOffset[1]];
const artboard = container(artboardSize, [0, 0, artboardSize[0], artboardSize[1]]);
artboard.createSvg(wrapper, [0, 0], true, "visible", "artboard", 0);
artboard.createDefs();
artboard.rect([0, 0], artboardSize, "white", "none", 0, "round", "artboard__background", "");

// EFFECT ---------------------------------------
const filterOffset = 1; // 1
let displacementFilter1 = effect();
displacementFilter1.createFilter(artboard, "displacementFilter-1");
// was soll mode?
displacementFilter1.turbulence("turbulence", 0.400012 + Math.random() * 0.01, 3, "turbulence", "multiply");
displacementFilter1.displacementMap("SourceGraphic", "turbulence", 3, "R", "G");

// COMPOSITION VALUES -----------------------------------
// 480,700
const compX = artboardSize[0] / 2 - compSize[0] / 2;
const compY = artboardSize[1] / 2 - compSize[1] / 2;

const cellSize = [20, 20]; // 20,50
const cells = [30, 30]; // 23,14

const arcSize = [cellSize[0] / 2, cellSize[1] / 2];
const a_tr = arc("a", [[arcSize[0], arcSize[1]], 0, 0, 0, [arcSize[0], arcSize[1]]], "a_tr");
const a_tr_l = arc("a", [[arcSize[0], arcSize[1]], 0, 1, 0, [arcSize[0], arcSize[1]]], "a_tr_l");
const a_br = arc("a", [[arcSize[0], arcSize[1]], 0, 0, 0, [-arcSize[0], arcSize[1]]], "a_br");
const a_br_l = arc("a", [[arcSize[0], arcSize[1]], 0, 1, 0, [-arcSize[0], arcSize[1]]], "a_br_l");
const a_bl = arc("a", [[arcSize[0], arcSize[1]], 0, 0, 0, [-arcSize[0], -arcSize[1]]], "a_bl");
const a_bl_l = arc("a", [[arcSize[0], arcSize[1]], 0, 1, 0, [-arcSize[0], -arcSize[1]]], "a_bl_l");
const a_tl = arc("a", [[arcSize[0], arcSize[1]], 0, 0, 0, [arcSize[0], -arcSize[1]]], "a_tl");
const a_tl_l = arc("a", [[arcSize[0], arcSize[1]], 0, 1, 0, [arcSize[0], -arcSize[1]]], "a_tl_l");

const mazePosition0 = [0, 0];
let mazeStart;
let traceStart;
let traceEnd;
let moveTo;
let prevFirst;
let nextLast;

const path0 = mazePath();
path0.createGrid(cells, cellSize); // einige Parameter aus drawGrid müssen wohl hier rüber nach createGrid

// SPLIT MAZE ------------------------------------
// Aufsplitten in initMaze & initTrace & initPath
const initClosedPath = (splitDirection, splitIndex, start, end) => {
  if (splitDirection === "ltr") {
    mazeStart = [start, splitIndex];

    for (let i = start; i < end; i++) {
      let row = path0.grid[i];
      row[splitIndex].static[2] = true;
      row[splitIndex + 1].static[0] = true;
    }

    traceStart = mazeStart;
    traceEnd = [start, splitIndex + 1];

    moveTo = "bottom";
    prevFirst = "bottom";
    nextLast = "top";
  }
  if (splitDirection === "rtl") {
    mazeStart = [end, splitIndex + 1];

    for (let i = start; i <= end; i++) {
      let row = path0.grid[i];
      row[splitIndex].static[2] = true;
      row[splitIndex + 1].static[0] = true;
    }

    traceStart = mazeStart;
    traceEnd = [end, splitIndex];

    moveTo = "top";
    prevFirst = "top";
    nextLast = "bottom";
  }
  if (splitDirection === "ttb") {
    mazeStart = [splitIndex + 1, start];

    let column1 = path0.grid[splitIndex];
    let column2 = path0.grid[splitIndex + 1];
    for (let i = start; i < end; i++) {
      column1[i].static[1] = true;
      column2[i].static[3] = true;
    }

    traceStart = mazeStart;
    traceEnd = [splitIndex, start];

    moveTo = "left";
    prevFirst = "left";
    nextLast = "right";
  }
  if (splitDirection === "btt") {
    mazeStart = [splitIndex, start];

    let column1 = path0.grid[splitIndex];
    let column2 = path0.grid[splitIndex + 1];
    for (let i = end; i <= start; i++) {
      column1[i].static[1] = true;
      column2[i].static[3] = true;
    }

    traceStart = mazeStart;
    traceEnd = [splitIndex + 1, start];

    moveTo = "right";
    prevFirst = "right";
    nextLast = "left";
  } else {
    console.error("unknown split command for maze");
  }
};
// initClosedPath('rtl', cells[1]-2, 21, 22) // y cells[0]-2 // x cells[1]-1

// initClosedPath("btt", cells[0] / 2 - 1, cells[1] - 1, cells[1] - 2);
initClosedPath("ttb", cells[0] / 2 - 1, 0, 1);

// MAZE
path0.createMaze(mazeStart);
path0.createTrace(traceStart, traceEnd);
// PATH
path0.createSegment(moveTo, prevFirst, nextLast, { largeChance: 2, evadeSelf: true, evadeEdges: true });
// path0.segment.addCommands([a_tl, a_tl, a_tl])
path0.segment.joinDs();

// DRAWING ------------------------------------
const comp = container(compSize, [0, 0, compSize[0], compSize[1]]);
comp.createSvg(artboard.svg, [compX, compY], false, "visible", "copath0", 0);
comp.rect([0, 0], compSize, "#c3c643", "none", 0, "round", "copath0__background", undefined); // '#3c6c43'

path0.drawMaze(comp, compSize, mazePosition0, "maze", { overflow: "visible", showCells: false, showStart: false, showWalls: false });
path0.drawTrace({ showTrace: false, showStart: false, showEnd: false });

// Styling evtl. auslagern ?
const drawSpace = container(compSize, [0, 0, compSize[0], compSize[1]]);
drawSpace.createSvg(comp.svg, [0, 0], false, "hidden", "drawSpace", 0);
drawSpace.rect([0, 0], compSize, "none", "#fe2", 0, "round", "drawSpace__background", undefined); // '#3c6c43'
drawSpace.path(path0.segment, "none", "#111", 1, "round", "round", "trace", undefined);

// let p = document.getElementsByClassName("trace")[0];
// console.log(p.getBoundingClientRect().width, p.getBoundingClientRect().height);

// const wallsStatic = (direction) => {
//   let position = 7;
//   if (direction === "x") {
//     for (let i = 1; i < path0.grid.length - 1; i++) {
//       let row = path0.grid[i];
//       row[position - 1].static[2] = true;
//       row[position].static[0] = true;
//     }
//   } else if (direction === "y") {
//     let column1 = path0.grid[position];
//     let column2 = path0.grid[position + 1];
//     for (let i = 1; i < column1.length - 1; i++) {
//       column1[i].static[1] = true;
//       column2[i].static[3] = true;
//     }
//   }
// };
// wallsStatic('y')

// const closedPathMaze = (splitDirection, splitIndex, start, end) => {
//   if (splitDirection === "ltr") {
//     mazeStart = [start, splitIndex];
//     traceStart = mazeStart;
//     traceEnd = [start, splitIndex + 1];

//     moveTo = "bottom";
//     prevFirst = "bottom";
//     nextLast = "top";

//     for (let i = start; i < end; i++) {
//       let row = path0.grid[i];
//       row[splitIndex].static[2] = true;
//       row[splitIndex + 1].static[0] = true;
//     }
//   } else if (splitDirection === "rtl") {
//     mazeStart = [end, splitIndex + 1];
//     traceStart = mazeStart;
//     traceEnd = [end, splitIndex];

//     moveTo = "top";
//     prevFirst = "top";
//     nextLast = "bottom";

//     for (let i = start; i <= end; i++) {
//       let row = path0.grid[i];
//       row[splitIndex].static[2] = true;
//       row[splitIndex + 1].static[0] = true;
//     }
//   } else if (splitDirection === "ttb") {
//     mazeStart = [splitIndex + 1, start];
//     traceStart = mazeStart;
//     traceEnd = [splitIndex, start];

//     moveTo = "left";
//     prevFirst = "left";
//     nextLast = "right";

//     let column1 = path0.grid[splitIndex];
//     let column2 = path0.grid[splitIndex + 1];
//     for (let i = start; i < end; i++) {
//       column1[i].static[1] = true;
//       column2[i].static[3] = true;
//     }
//   } else if (splitDirection === "btt") {
//     mazeStart = [splitIndex, end];
//     traceStart = mazeStart;
//     traceEnd = [splitIndex + 1, end];

//     moveTo = "right";
//     prevFirst = "right";
//     nextLast = "left";

//     let column1 = path0.grid[splitIndex];
//     let column2 = path0.grid[splitIndex + 1];
//     for (let i = start; i <= end; i++) {
//       column1[i].static[1] = true;
//       column2[i].static[3] = true;
//     }
//   } else {
//     console.error("unknown split command for maze");
//   }
// };
