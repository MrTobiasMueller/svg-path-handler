// COMMANDS -------------------------------------
const dUpdater = (state) => ({
  updateD() {
    // for (command of commands) {
    console.log(state.type);
    if (state.type == "M" || state.type == "m") {
      state.d = ` ${state.type} ${state.position[0]} ${state.position[1]}`;
    } else if (state.type == "A" || state.type == "a") {
      state.d = ` ${state.type}` + ` ${state.radius[0]} ${state.radius[1]}` + ` ${state.angle} ${state.largeArc} ${state.sweep}` + ` ${state.endPoint[0]} ${state.endPoint[1]}`;
    }
    // }
  },
});

const move = (type, values, name) => {
  const self = {
    type,
    values,
    name,
    position: [values[0][0], values[0][1]],
    d: ` ${type} ${values[0][0]} ${values[0][1]}`,
  };
  return Object.assign(self, dUpdater(self));
};

const arc = (type, values, name) => {
  const self = {
    type,
    values,
    radius: [values[0][0], values[0][1]],
    angle: values[1],
    largeArc: values[2],
    sweep: [values[3]],
    endPoint: [values[4][0], values[4][1]],
    name,
    d: ` ${type}` + ` ${values[0][0]} ${values[0][1]}` + ` ${values[1]} ${values[2]} ${values[3]}` + ` ${values[4][0]} ${values[4][1]}`,
  };
  return Object.assign(self, dUpdater(self));
};