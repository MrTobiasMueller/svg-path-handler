// SEGMENT -------------------------------------

// So wie es aussieht, brauchen die commands createD()
// Eventuell muss createD() state.d überschreiben > changeD()
/*const commandsMirrorer = state => ({
  mirrorCommands(axis) {
    for (command of state.commands) {
      if (command.type == 'a') {

      }
    }
  }
})*/
const commandsAdder = (state) => ({
  addCommands(commands) {
    for (command of commands) {
      state.commands.push(command);
    }
  },
});
const dsJoiner = (state) => ({
  joinDs() {
    const ds = state.commands.map(({ d }) => d);
    state.d = ds.reduce((prevVal, currVal, idx) => {
      return idx == 0 ? currVal : prevVal + currVal;
    });
  },
});
const segmentBehaviors = (state) => Object.assign(commandsAdder(state), dsJoiner(state));
const segment = (name) => {
  const self = {
    name,
    type: "segment",
    commands: [],
  };
  return Object.assign(self, segmentBehaviors(self));
};