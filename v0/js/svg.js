// FILTER -------------------------------------
const turbulenceCreater = (state) => ({
  turbulence(type, baseFrequency, numOctaves, result, mode) {
    const turbulence = document.createElementNS(ns, "feTurbulence");
    turbulence.setAttribute("type", type);
    turbulence.setAttribute("baseFrequency", baseFrequency);
    turbulence.setAttribute("numOctaves", numOctaves);
    turbulence.setAttribute("result", result);
    turbulence.setAttribute("mode", mode);
    state.filter.appendChild(turbulence);
  },
});

const displacementMapCreater = (state) => ({
  displacementMap(_in, _in2, scale, xChannelSelector, yChannelSelector) {
    const displacementMap = document.createElementNS(ns, "feDisplacementMap");
    displacementMap.setAttribute("in", _in);
    displacementMap.setAttribute("in2", _in2);
    displacementMap.setAttribute("scale", scale);
    displacementMap.setAttribute("xChannelSelector", xChannelSelector);
    displacementMap.setAttribute("yChannelSelector", yChannelSelector);
    state.filter.appendChild(displacementMap);
  },
});

const filterCreater = (state) => ({
  createFilter(parent, id) {
    state.filter = document.createElementNS(ns, "filter");
    state.filter.setAttribute("id", id);
    parent.defs.appendChild(state.filter);
  },
});

const effect = () => {
  const self = {};
  return Object.assign(self, filterCreater(self), turbulenceCreater(self), displacementMapCreater(self));
};

// CONTAINER -------------------------------------
const ns = "http://www.w3.org/2000/svg";
const nsLink = "http://www.w3.org/1999/xlink";
const svgCreater = (state) => ({
  // HIER AUCH FILTEROFFSET?
  // IST VLT BESSER, ALS ES IN CREATE VON MAZEPATH ZU REGELN…
  createSvg(parent, position, root, overflow, classNames, offset) {
    const svg = document.createElementNS(ns, "svg");
    state.position = position;
    state.parent = parent;
    state.svg = svg;
    if (root) {
      svg.setAttribute("xmlns", ns);
      svg.setAttribute("xmlns:xlink", nsLink);
    }
    svg.setAttribute("x", state.position[0]);
    svg.setAttribute("y", state.position[1]);
    svg.setAttribute("width", state.size[0]);
    svg.setAttribute("height", state.size[1]);
    // Hat eine verschachtelte Svg überhaupt eine viewBox?
    svg.setAttribute("viewBox", `0 0 ${state.size[0]} ${state.size[1]}`);
    svg.setAttribute("style", `overflow: ${overflow};`);
    if (classNames) svg.setAttribute("class", classNames);
    //svg.classList.add('')
    parent.appendChild(svg);
    if (offset && offset > 0) svg.setAttribute("transform", `translate(${-offset}, ${-offset})`);
  },
});
const defsCreater = (state) => ({
  createDefs() {
    const defs = document.createElementNS(ns, "defs");
    state.defs = defs;
    state.svg.appendChild(defs);
  },
});
const rectCreater = (state) => ({
  rect(position, size, fill, stroke, strokeWidth, linejoin, classNames, filter) {
    const rect = document.createElementNS(ns, "rect");
    rect.setAttribute("x", position[0]);
    rect.setAttribute("y", position[1]);
    rect.setAttribute("width", size[0]);
    rect.setAttribute("height", size[1]);
    rect.setAttribute("fill", fill);
    rect.setAttribute("stroke", stroke);
    rect.setAttribute("stroke-width", strokeWidth);
    rect.setAttribute("stroke-linejoin", linejoin);
    if (classNames) rect.setAttribute("class", classNames);
    if (filter) rect.setAttribute("filter", `url(${filter})`);
    state.svg.appendChild(rect);
  },
});
const lineCreater = (state) => ({
  line(position1, position2, stroke, strokeWidth, lineCap, classNames, filter) {
    const line = document.createElementNS(ns, "line");
    line.setAttribute("x1", position1[0]);
    line.setAttribute("y1", position1[1]);
    line.setAttribute("x2", position2[0]);
    line.setAttribute("y2", position2[1]);
    line.setAttribute("stroke", stroke);
    line.setAttribute("stroke-width", strokeWidth);
    line.setAttribute("stroke-linecap", lineCap);
    if (classNames) line.setAttribute("class", classNames);
    if (filter) line.setAttribute("filter", `url(${filter})`);
    state.svg.appendChild(line);
  },
});
const pathCreater = (state) => ({
  path(segment, fill, stroke, strokeWidth, lineCap, lineJoin, classNames, filter) {
    const path = document.createElementNS(ns, "path");
    path.setAttribute("d", segment.d);
    path.setAttribute("fill", fill);
    path.setAttribute("stroke", stroke);
    path.setAttribute("stroke-width", strokeWidth);
    path.setAttribute("stroke-linecap", lineCap);
    path.setAttribute("stroke-linejoin", lineJoin);
    if (classNames) path.setAttribute("class", classNames);
    if (filter) path.setAttribute("filter", `url(${filter})`);
    path.setAttribute("pathLength", 100);
    state.svg.appendChild(path);
  },
});
const containerBehaviors = (state) => Object.assign(svgCreater(state), defsCreater(state), pathCreater(state), rectCreater(state), lineCreater(state));
const container = (size, viewBox) => {
  const self = {
    size,
    viewBox,
  };
  return Object.assign(self, containerBehaviors(self));
};