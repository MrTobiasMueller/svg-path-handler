// GRID -------------------------------------------
const gridCreater = (state) => ({
  createGrid(cells, cellSize) {
    state.cells = cells;
    state.grid = new Array(state.cells[0]);
    state.cellSize = cellSize;

    for (let i = 0; i < state.grid.length; i++) {
      state.grid[i] = new Array(state.cells[1]);
    }
    for (let i = 0; i < state.grid.length; i++) {
      for (let j = 0; j < state.cells[1]; j++) {
        const xPosition = i * cellSize[0];
        const yPosition = j * cellSize[1];
        state.grid[i][j] = cell(i, j, cellSize[0], cellSize[1], xPosition, yPosition);
      }
    }

  },
});

// MAZE -------------------------------------------
const mazeCreater = (state) => ({
  createMaze(mazeStart) {
    state.mazeStart = mazeStart;
    let finished = false;
    let stack = [];
    let current = state.grid[state.mazeStart[0]][state.mazeStart[1]];
    while (finished === false) {
      current.visited = true;
      // STEP 1
      const next = current.checkNeighbors(state);
      if (next) {
        next.visited = true;
        // STEP 2
        stack.push(current);
        // STEP 3
        removeWalls(current, next);
        // STEP 4
        current = next;
      } else if (stack.length > 0) {
        current = stack.pop();
      } else {
        finished = true;
        // console.log('created maze')
      }
    }
  },
});

const mazeDrawer = (state) => ({
  drawMaze(parent, mazeSize, mazePosition, classNames, mode) {
    state.parent = parent;
    state.mazeSize = mazeSize;
    state.mazePosition = mazePosition;
    state.classNames = classNames;
    state.mazeMode = mode;

    if (state.mazeMode.showStart || state.mazeMode.showCells || state.mazeMode.showWalls) {
      state.container = container(state.mazeSize, [0, 0, state.mazeSize[0], state.mazeSize[1]]);
      state.container.createSvg(parent.svg, state.mazePosition, 'false', state.mazeMode.overflow, state.mazeMode.classNames, 0);
    }

    if (state.mazeMode.showCells) {
      // if (showWalls) { // wieso showWalls() ?
      for (let i = 0; i < state.cells[0]; i++) {
        for (let j = 0; j < state.cells[1]; j++) {
          state.grid[i][j].highlight(state.container, "#aaa", "maze__cell");
        }
      }
      // }
    }

    if (state.mazeMode.showStart) {
      state.grid[state.mazeStart[0]][state.mazeStart[1]].highlight(state.container, "#aaf", "maze__start");
    }

    if (state.mazeMode.showWalls) {
      for (let i = 0; i < state.cells[0]; i++) {
        for (let j = 0; j < state.cells[1]; j++) {
          state.grid[i][j].drawWalls(state.container, state.cellSize);
        }
      }
    }
  },
});

// FIND TRACE -------------------------------------------
const traceCreater = (state) => ({
  createTrace(traceStart, traceEnd) {
    state.traceStart = state.grid[traceStart[0]][traceStart[1]];
    state.traceEnd = state.grid[traceEnd[0]][traceEnd[1]];
    let openSet = [];
    let closedSet = [];
    let noSolution = false;

    for (let i = 0; i < state.cells[0]; i++) {
      for (let j = 0; j < state.cells[1]; j++) {
        state.grid[i][j].addNeighbors(state);
      }
    }
    openSet.push(state.traceStart);
    if (openSet.length > 0) {
      while (openSet.length > 0) {
        let winningIndex = 0;
        for (let i = 0; i < openSet.length; i++) {
          // Finde den WinningIndex
          if (openSet[i].f < openSet[winningIndex].f) {
            winningIndex = i;
          }
        }

        let current = openSet[winningIndex];

        if (current === state.traceEnd) {
          // FIND THE TRACE
          if (!noSolution) {
            state.reversedTrace = [];
            // 1. Put the end in the list…
            let temp = current;
            temp.blocked = true;
            state.reversedTrace.push(temp);
            while (temp.previous) {
              temp.previous.blocked = true;
              // 2. The end is connected to the one before, so that goes in the list…
              state.reversedTrace.push(temp.previous);
              temp = temp.previous;
            }
          }
          console.log("DONE!");
        }

        removeFromArray(openSet, current);
        closedSet.push(current);

        // Bevor die neighbors in openSet gepushed
        // werden können, müssen sie evaluiert werden.
        let neighbors = current.mazeNeighbors;

        for (let i = 0; i < neighbors.length; i++) {
          let neighbor = neighbors[i];

          let newTrace = false;
          if (!closedSet.includes(neighbor)) {
            let tempG = current.g + 1;
            // Wurde der Nachbar bereits evaluiert?
            if (openSet.includes(neighbor)) {
              // Ist dies ein besseres g?
              if (tempG < neighbor.g) {
                neighbor.g = tempG;
                newTrace = true;
              }
              // Wenn er noch nicht evaluiert wurde…
            } else {
              neighbor.g = tempG;
              newTrace = true;
              openSet.push(neighbor);
            }

            if (newTrace) {
              neighbor.h = heuristic(neighbor, traceEnd);
              neighbor.f = neighbor.g + neighbor.h;
              // Wieso sind f und h NaN?
              neighbor.previous = current;
            }
          }
        }
      }
    } else {
      console.log("no solution for trace");
      noSolution = true;
    }
  },
});

const traceDrawer = (state) => ({
  drawTrace(mode) {
    state.traceMode = mode
    if (state.reversedTrace) {
      if ((!state.container && mode.showTrace) || mode.showStart || mode.showEnd) {
        state.container = container(state.mazeSize, [0, 0, state.mazeSize[0], state.mazeSize[1]]);
        state.container.createSvg(state.parent.svg, state.mazePosition, state.root, state.overflow, state.classNames, 0);
      }

      if (mode.showTrace) {
        for (let i = 0; i < state.reversedTrace.length; i++) {
          state.reversedTrace[i].highlight(state.container, "#eee", "trace__cell");
        }
      }
      if (mode.showStart) {
        state.traceStart.highlight(state.container, "#afa", "trace__end");
      }
      if (mode.showEnd) {
        state.traceEnd.highlight(state.container, "#faa", "trace__end");
      }
    } else {
      console.log("no trace has been found");
    }
  },
});

// CREATE SEGMENT -------------------------------------------
const segmentCreater = (state) => ({
  createSegment(moveTo, prevFirst, nextLast, mode) {
    const trace = state.reversedTrace.reverse();
    state.segmentMode = mode;
    state.segment = segment("arcs");

    // let testCommands = []
    let pathFinished = false;

    for (let i = 0; i <= trace.length; i++) {
      let current = trace[i];
      // SET LARGE SEGMENT
      let rand = getRandomInt(1, 10);
      if (rand <= state.segmentMode.largeChance) {
        current.largeSegment = true;
      } else {
        current.largeSegment = false;
      }
      // EVADE EDGES
      if (state.segmentMode.evadeEdges) {
        if (current.x === 0 || current.x === state.cells[0] - 1 || current.y === 0 || current.y === state.cells[1] - 1) {
          current.largeSegment = false;
        }
      }

      current.prev = trace[i - 1];
      current.next = trace[i + 1];

      // PREV -----------------------------------------
      if (current.prev) {
        if (current.prev.y < current.y) {
          current.prevTop = true;
        }
        if (current.prev.x > current.x) {
          current.prevRight = true;
        }
        if (current.prev.y > current.y) {
          current.prevBottom = true;
        }
        if (current.prev.x < current.x) {
          current.prevLeft = true;
        }
      } else {
        // HANDLE FIRST COMMAND
        // console.log('no prev')
        if (prevFirst === "top") {
          current.prevTop = true;
        } else if (prevFirst === "right") {
          current.prevRight = true;
        } else if (prevFirst === "bottom") {
          current.prevBottom = true;
        } else if (prevFirst === "left") {
          current.prevLeft = true;
        } else {
          current.prevBottom = true;
        }
      }

      // NEXT -----------------------------------------
      if (current.next) {
        if (current.next.y < current.y) {
          current.nextTop = true;
        }
        if (current.next.x > current.x) {
          current.nextRight = true;
        }
        if (current.next.y > current.y) {
          current.nextBottom = true;
        }
        if (current.next.x < current.x) {
          current.nextLeft = true;
        }
      } else {
        // HANDLE LAST COMMAND
        // console.log('no next')
        if (nextLast === "top") {
          current.nextTop = true;
        } else if (nextLast === "right") {
          current.nextRight = true;
        } else if (nextLast === "bottom") {
          current.nextBottom = true;
        } else if (nextLast === "left") {
          current.nextLeft = true;
        } else {
          current.nextBottom = true;
        }
        pathFinished = true;
      }

      // ADD COMMANDS TO SEGMENT BASE ON PREV AND LEFT
      current.applyGrammar();

      // console.log(current.segment.name)

      if (pathFinished) {
        console.log(state.segment)
        break;
      }
    }

    // CHECK TOUCH FOR ABSOLUTE NEIGHBORS FOR EVERY CELL ------------
    // IST DAS HIER DER MODUS CLOSED-PREVENT-FORCE-BOTH?
    for (let i = 0; i < state.grid.length; i++) {
      for (let j = 0; j < state.cells[1]; j++) {
        const current = state.grid[i][j];

        if (current.segment) {
          if (current.y > 0) {
            current.absoluteTop = state.grid[current.x][current.y - 1];
          }
          if (current.x < state.cells[0] - 1) {
            current.absoluteRight = state.grid[current.x + 1][current.y];
          }
          if (current.y < state.cells[1] - 1) {
            current.absoluteBottom = state.grid[current.x][current.y + 1];
          }
          if (current.x > 0) {
            current.absoluteLeft = state.grid[current.x - 1][current.y];
          }

          // EVADE SELF -------------------------------------------------------------------------------
          if (mode.evadeSelf) {
            let rand = getRandomInt(0, 1);
            // ABSOLUTE TOP
            if (current.largeSegment && current.absoluteTop && current.absoluteTop.largeSegment) {
              if (
                current.segment.name != "tr_concave" && 
                current.absoluteTop.segment.name != "tr_concave" && 
                current.segment.name != "br_concave" && 
                current.absoluteTop.segment.name != "br_concave" && 
                current.segment.name != "bl_concave" && 
                current.absoluteTop.segment.name != "bl_concave" && 
                current.segment.name != "tl_concave" && 
                current.absoluteTop.segment.name != "tl_concave" && 
                current.segment.name != "cr" && 
                current.absoluteTop.segment.name != "cr" && 
                current.segment.name != "cl" && 
                current.absoluteTop.segment.name != "cl"
              ) {
                if (!(current.segment.name === "cb" && current.absoluteTop.segment.name === "ct")) {
                  if (rand === 0) {
                    current.largeSegment = false;
                    current.arcSegment(current.segment.name, current.largeSegment);
                  } else {
                    current.absoluteTop.largeSegment = false;
                    current.absoluteTop.arcSegment(current.absoluteTop.segment.name, current.absoluteTop.largeSegment);
                  }
                }
              }
            }
            // ABSOLUTE RIGHT
            if (
              current.largeSegment && 
              current.absoluteRight && 
              current.absoluteRight.largeSegment
            ) {
              if (
                current.segment.name != "tr_concave" && 
                current.absoluteRight.segment.name != "tr_concave" && 
                current.segment.name != "br_concave" && 
                current.absoluteRight.segment.name != "br_concave" && 
                current.segment.name != "bl_concave" && 
                current.absoluteRight.segment.name != "bl_concave" && 
                current.segment.name != "tl_concave" && 
                current.absoluteRight.segment.name != "tl_concave"
              ) {
                if (!(current.segment.name === "cl" && current.absoluteRight.segment.name === "cr")) {
                  if (rand === 0) {
                    current.largeSegment = false;
                    current.arcSegment(current.segment.name, current.largeSegment);
                  } else {
                    current.absoluteRight.largeSegment = false;
                    current.absoluteRight.arcSegment(current.absoluteRight.segment.name, current.absoluteRight.largeSegment);
                  }
                }
              }
            }
          } // if (mode.evadeSelf)
        }
      } // if (current.segment)
    }

    // GENERATE SEGMENT FROM SEGMENT IN CELLS ------------------------
    for (let i = 0; i < trace.length; i++) {
      state.segment.addCommands(trace[i].segment.commands);
      //testCommands.push('test')
    }

    // STARTPOINT -----------------------------------------
    let startPoint = setStartPoint(state.cellSize, trace[0], moveTo);
    if (startPoint) state.segment.commands.splice(0, 0, startPoint);

    // FOR DEBUGGING -----------------------------------------
    // console.log(testCommands.length)
    // console.log(trace.length)
  },
});

const mazePath = () => {
  const self = {
    // reversedTrace: []
  };
  return Object.assign(
    self, 
    gridCreater(self), 
    mazeCreater(self), 
    mazeDrawer(self), 
    traceCreater(self), 
    traceDrawer(self), 
    segmentCreater(self)
  );
};
