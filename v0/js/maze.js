const maze = () => {
  const self = {
    // reversedTrace: []
  };
  return Object.assign(
    self, 
    gridCreater(self), 
    mazeCreater(self), 
    mazeDrawer(self), 
    traceCreater(self), 
    traceDrawer(self), 
    segmentCreater(self)
    );
};

//